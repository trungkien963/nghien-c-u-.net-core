﻿using Couchbase;
using Couchbase.N1QL;
using DMS.Core.Models;
using DMS.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMS.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private static CouchbaseDataAccess dataAccess = CouchbaseDataAccess.Instance;
        private Couchbase.Core.IBucket bucket = dataAccess.EmployeeBucket;

        public async Task<int> DeleteEmployeeByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var result = await bucket.RemoveAsync(id.ToString());
            return result.Success ? 1 : 0;
        }

        public async Task<Employee> GetEmployeeByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var statement = $"SELECT bucketname.* FROM {bucket.Name} AS bucketname WHERE `partition` = $_partition AND bucketname.id = $_id";
            var query = new QueryRequest(statement)
                            .AddNamedParameter("_id", id)
                            .AddNamedParameter("_partition", "Employee_PTK");
            var result = await bucket.QueryAsync<Employee>(query);
            return result.Rows.FirstOrDefault();
        }

        public async Task<List<Employee>> GetEmployeeAsync(CancellationToken cancellationToken)
        {
            var statement = $"SELECT bucketname.* FROM {bucket.Name} AS bucketname WHERE `partition` = $_partition ";
            var query = new QueryRequest(statement)
                            .AddNamedParameter("_partition", "Employee_PTK");

            var result = await bucket.QueryAsync<Employee>(query);
            return result.Rows;
        }

        public async Task<Employee> InsertEmployeeAsync(Employee employee, CancellationToken cancellationToken)
        {
            var document = new Document<Employee>
            {
                Content = employee,
                Id = employee.Id.ToString()
            };
            var result = await bucket.InsertAsync(document);
            return result.Success ? employee : null;
        }


        public async Task<Employee> UpdateEmployeeAsync(Employee employee, CancellationToken cancellationToken)
        {
            var empl = await GetEmployeeByIdAsync(employee.Id, cancellationToken);
            if (empl == null) return null;
            else employee = employee.MixBaseObject(empl);

            var document = new Document<Employee>
            {
                Content = employee,
                Id = employee.Id.ToString()
            };
            var result = await bucket.UpsertAsync(document);
            return result.Success ? employee : null;
        }
    }
}
