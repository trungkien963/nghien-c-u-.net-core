﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Shared
{
    public class OcrSetting
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Hosts { get; set; }
    }
    public class AmazonS3Setting
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Hosts { get; set; }
    }
}
