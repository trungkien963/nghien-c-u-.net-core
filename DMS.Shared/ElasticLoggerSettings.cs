﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Shared
{
    public class ElasticLoggerSettings
    {
        public int Port { get; set; }
        public string Hosts { get; set; }
    }
}
