﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Shared
{
    public class clsCommonInfoClass
    {
        public long Key { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public string Format { get; set; }
    }
}
