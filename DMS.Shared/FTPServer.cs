﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Shared
{
    public class FTPServer
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Hosts { get; set; }
        public string Option { get; set; }
    }
}
