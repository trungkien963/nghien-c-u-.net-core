﻿using DMS.Core.Models;
using DMS.Core.Repositories;
using DMS.Core.Services;
using DMS.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VNPT.Framework.CommonType;

namespace DMS.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        public EmployeeService(IEmployeeRepository employee)
        {
            _employeeRepository = employee;
        }

        public async Task<FunctionResult> CreateNewEmployeeAsync(Employee employee, CancellationToken cancellationToken)
        {
            var result = FunctionResult.Success;
            var value = await _employeeRepository.InsertEmployeeAsync(employee, cancellationToken);
            if (value == null) result.Error(ErrorCodeCollection.Employee.EmployeeCreateFail);
            result.SetData(value);
            return result;
        }

        public async Task<FunctionResult> DeleteEmployeeAsync(Guid id, CancellationToken cancellationToken)
        {

            var result = FunctionResult.Success;
            var value = await _employeeRepository.DeleteEmployeeByIdAsync(id, cancellationToken);
            if (value == 0) result.Error(ErrorCodeCollection.Employee.EmployeeDeleteFail);
            return result;
        }

        public async Task<Employee> GetEmployeeByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var empl = await _employeeRepository.GetEmployeeByIdAsync(id, cancellationToken);
            return empl;
        }

        public async Task<List<Employee>> GetListEmployeeAsync(CancellationToken cancellationToken)
        {
            var listEmployee = await _employeeRepository.GetEmployeeAsync(cancellationToken);
            return listEmployee;

        }

        public async Task<FunctionResult> UpdateEmployeeAsync(Employee employee, CancellationToken cancellationToken)
        {
            var result = FunctionResult.Success;
            var value = await _employeeRepository.UpdateEmployeeAsync(employee, cancellationToken);
            if (value == null) result.Error(ErrorCodeCollection.AccountError.AccountUpdateFailed);
            result.SetData(value);
            return result;
        }
    }
}
