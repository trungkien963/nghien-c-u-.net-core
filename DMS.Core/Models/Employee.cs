﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Core.Models
{
    public class Employee : BaseModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string Birth { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public override string Partition => "Employee_PTK";
        public Employee MixBaseObject(Employee employee)
        {
            var empl = employee;
            if (!String.IsNullOrEmpty(Name)) empl.Name = Name;
            if (!String.IsNullOrEmpty(Code)) empl.Code = Code;
            if (!String.IsNullOrEmpty(Address)) empl.Address = Address;
            if (!String.IsNullOrEmpty(Birth)) empl.Birth = Birth;
            if (!String.IsNullOrEmpty(Email)) empl.Email = Email;
            if (!String.IsNullOrEmpty(Image)) empl.Image = Image;
            return empl;
        }

    }
}
