﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DMS.Core.Models;
using VNPT.Framework.CommonType;

namespace DMS.Core.Services
{
    public interface IEmployeeService
    {
        Task<FunctionResult> CreateNewEmployeeAsync(Employee employee, CancellationToken cancellationToken);
        Task<FunctionResult> UpdateEmployeeAsync(Employee employee, CancellationToken cancellationToken);
        Task<FunctionResult> DeleteEmployeeAsync(Guid id, CancellationToken cancellationToken);
        Task<Employee> GetEmployeeByIdAsync(Guid id, CancellationToken cancellationToken);
        Task<List<Employee>> GetListEmployeeAsync(CancellationToken cancellationToken);
        //Task<FunctionResult> CreateListDanhMucTinhThanhPhoAsync(List<DanhMucTinhThanhPho> listDanhMuc, CancellationToken cancellationToken);

    }
}
