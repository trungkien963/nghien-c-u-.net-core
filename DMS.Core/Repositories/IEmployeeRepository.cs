﻿using DMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMS.Core.Repositories
{
    public interface IEmployeeRepository
    {
        Task<Employee> InsertEmployeeAsync(Employee employee, CancellationToken cancellationToken);
        Task<Employee> UpdateEmployeeAsync(Employee employee, CancellationToken cancellationToken);
        Task<int> DeleteEmployeeByIdAsync(Guid id, CancellationToken cancellationToken);
        Task<Employee> GetEmployeeByIdAsync(Guid id, CancellationToken cancellationToken);
        Task<List<Employee>> GetEmployeeAsync(CancellationToken cancellationToken);
        //Task<int> InsertMultiDanhMucTinhThanhPhoAsync(List<DanhMucTinhThanhPho> listDanhMuc, CancellationToken cancellationToken);
    }
}
