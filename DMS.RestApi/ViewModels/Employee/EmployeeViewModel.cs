﻿
using DMS.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMS.RestApi.ViewModels
{
    public class EmployeeViewModel : BaseViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string Birth { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public DMS.Core.Models.Employee InsertGetModel()
        {
            var employee = new DMS.Core.Models.Employee
            {
                Id = Guid.NewGuid(),
                Name = Name,
                Code = Code,
                Address = Address,
                Birth = Birth,
                Email = Email,
                Image = Image
            };
            return employee;
        }
        public DMS.Core.Models.Employee UpdateGetModel()
        {
            var employee = new DMS.Core.Models.Employee{
                Id = ID,
                Name = Name,
                Code = Code,
                Address = Address,
                Birth = Birth,
                Email = Email,
                Image = Image
            };
            return employee;
        }
        
    }
}
