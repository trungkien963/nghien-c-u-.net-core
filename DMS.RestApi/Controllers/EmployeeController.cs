﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using DMS.Workflows.EmployeeWorkflows;
using DMS.RestApi.ViewModels;
using DMS.Core.Models;
using DMS.Shared;

namespace DMS.RestApi.Controllers
{
    [Route("api/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetListEmployeeAsync(CancellationToken cancellationToken)
        {
            var token = Request.GetTokenValue();
            var employeelist = new EmployeeGetListByTokenWorkflows("", token);
            var (lastestResult, details) = await employeelist.ExecuteAsync("", token, cancellationToken);
            if (details[0].Failed) return BadRequest(details[0]);
            var resultStores = details[0].GetData<List<Employee>>();

            if (resultStores is null) return BadRequest(ErrorCodeCollection.Valid.DataResultIsNull);
            return Ok(resultStores);
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewEmployeeAsync([FromBody] EmployeeViewModel model, CancellationToken cancellationToken)
        {
            var token = Request.GetTokenValue();
            var employee = model.InsertGetModel();
            var empl = new EmployeeCreateNewWorkflows("", employee, token);
            var (lastestResult, details) = await empl.ExecuteAsync("", token, cancellationToken);
            if (details[0].Failed) return BadRequest(details[0]);
            var result = details[0].GetData<Employee>();
            if (result is null) return BadRequest(ErrorCodeCollection.Employee.EmployeeCreateFail);
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdateEmployeeAsync([FromRoute] Guid id, [FromBody] EmployeeViewModel model, CancellationToken cancellationToken)
        {
            var token = Request.GetTokenValue();
            var empl = model.UpdateGetModel();
            var employeeWorkflow = new EmployeeUpdateWorkflows("", token, empl);
            var (lastestResult, details) = await employeeWorkflow.ExecuteAsync("", token, cancellationToken);
            if (details[0].Failed) return BadRequest(details[0]);

            var result = details[0].GetData<Employee>();
            if (result is null) return BadRequest(ErrorCodeCollection.Employee.EmployeeUpdateFail);
            return Ok(result);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteEmployeeAsync([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var token = Request.GetTokenValue();
            var employeeDeleteWorkflow = new EmployeeDeleteWorkflows("", token, id);
            var (lastestResult, details) = await employeeDeleteWorkflow.ExecuteAsync("delete", token, cancellationToken);
            if (details[0].Failed) return BadRequest(details[0]);
            return Ok(details[0]);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetEmployeeByIdAsync([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var token = Request.GetTokenValue();
            var employeeGetObjectByIdWorkflows = new EmployeeGetObjectByIdWorkflows("", token, id);
            var (lastestResult, details) = await employeeGetObjectByIdWorkflows.ExecuteAsync("", token, cancellationToken);
            if (details[0].Failed) return BadRequest(details[0]);
            var result = details[0].GetData<Employee>();
            if (result is null) return BadRequest(ErrorCodeCollection.Valid.DataResultIsNull);
            return Ok(result);
        }
    }
}
