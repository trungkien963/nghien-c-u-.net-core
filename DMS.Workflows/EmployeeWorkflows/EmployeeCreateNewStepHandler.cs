﻿using DMS.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VNPT.Framework.CommonType;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeCreateNewStepHandler : BaseStepHandler
    {
        private readonly IEmployeeService _employeeService;
        private readonly DMS.Core.Models.Employee _employee;
        public EmployeeCreateNewStepHandler(DMS.Core.Models.Employee employee)
        {

            _employeeService = DependencyResolution.Resolve<IEmployeeService>();
            _employee = employee;
        }
        public override bool Required => true;

        public override async Task<FunctionResult> ExecuteAsync(FunctionResult previousResults, CancellationToken cancellationToken)
        {
            var result = await _employeeService.CreateNewEmployeeAsync(_employee, cancellationToken);
            return result;
        }
    }
}
