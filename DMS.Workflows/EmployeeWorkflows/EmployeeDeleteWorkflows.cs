﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeDeleteWorkflows : BaseWorkflow
    {
        public EmployeeDeleteWorkflows(string pathApi, string token, Guid id)
        {
            //AddHandler(new TokenValidateStepHandler(pathApi,token));
            AddHandler(new EmployeeDeleteStepHandler(id));
        }
    }
}
