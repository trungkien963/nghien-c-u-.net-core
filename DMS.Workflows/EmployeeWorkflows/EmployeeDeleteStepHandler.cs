﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VNPT.Framework.CommonType;
using DMS.Core.Services;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeDeleteStepHandler : BaseStepHandler
    {
        private readonly IEmployeeService _employeeService;
        private readonly Guid id;
        public EmployeeDeleteStepHandler(Guid _idEmployee)
        {
            id = _idEmployee;
            _employeeService = DependencyResolution.Resolve<IEmployeeService>();
        }
        public override bool Required => true;

        public override async Task<FunctionResult> ExecuteAsync(FunctionResult previousResults, CancellationToken cancellationToken)
        {
            var result = await _employeeService.DeleteEmployeeAsync(id, cancellationToken);
            return result;
        }
    }
}
