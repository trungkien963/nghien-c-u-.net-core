﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeGetObjectByIdWorkflows : BaseWorkflow
    {
        public EmployeeGetObjectByIdWorkflows(string pathApi, string token,Guid id)
        {
            //AddHandler(new TokenValidateStepHandler(pathApi,token));
            AddHandler(new EmployeeGetObjectByIdStepHandler(id));
        }
    }
}
