﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VNPT.Framework.CommonType;
using DMS.Core.Services;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeGetObjectByIdStepHandler : BaseStepHandler
    {
        private readonly IEmployeeService _employeeService;
        private readonly Guid _idEmployee;
        public EmployeeGetObjectByIdStepHandler(Guid id)
        {
            _idEmployee = id;
            _employeeService = DependencyResolution.Resolve<IEmployeeService>();
        }
        public override bool Required => true;

        public override async Task<FunctionResult> ExecuteAsync(FunctionResult previousResults, CancellationToken cancellationToken)
        {
            var result = FunctionResult.Success;
            var empl = await _employeeService.GetEmployeeByIdAsync(_idEmployee, cancellationToken);
            result.SetData(empl);
            return result;
        }
    }
}
