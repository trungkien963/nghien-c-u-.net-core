﻿using DMS.Workflows.EmployeeWorkflows;
using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeCreateNewWorkflows : BaseWorkflow
    {
        public EmployeeCreateNewWorkflows(string pathApi, DMS.Core.Models.Employee _employee, String token)
    {
        //AddHandler(new TokenValidateStepHandler(pathApi,token));
        AddHandler(new EmployeeCreateNewStepHandler(_employee));
    }
}
}
