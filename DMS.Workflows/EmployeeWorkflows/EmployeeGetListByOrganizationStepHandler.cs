﻿using DMS.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VNPT.Framework.CommonType;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeGetListByOrganizationStepHandler : BaseStepHandler
    {
        private readonly IEmployeeService _employeeService;
        private readonly string _token;
        public EmployeeGetListByOrganizationStepHandler(string token)
        {
            _token = token;
            _employeeService = DependencyResolution.Resolve<IEmployeeService>();
        }
        public override bool Required => true;

        public override async Task<FunctionResult> ExecuteAsync(FunctionResult previousResults, CancellationToken cancellationToken)
        {
            var result = FunctionResult.Success;
            var listEmployee = await _employeeService.GetListEmployeeAsync(cancellationToken);
            result.SetData(listEmployee);
            return result;
        }
    }
}
