﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeUpdateWorkflows : BaseWorkflow
    {
        public EmployeeUpdateWorkflows(string pathApi, string token, DMS.Core.Models.Employee employee)
        {
            //AddHandler(new TokenValidateStepHandler(pathApi,token));
            AddHandler(new EmployeeUpdateStepHandler(employee));
        }
    }
}
