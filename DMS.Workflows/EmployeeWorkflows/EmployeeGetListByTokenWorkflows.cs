﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeGetListByTokenWorkflows : BaseWorkflow
    {
        public EmployeeGetListByTokenWorkflows(string pathApi, string token)
        {
            //AddHandler(new TokenValidateStepHandler(pathApi,token));
            AddHandler(new EmployeeGetListByOrganizationStepHandler(token));
        }
    }
}
