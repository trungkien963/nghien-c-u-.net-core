﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VNPT.Framework.CommonType;
using DMS.Core.Services;
using DMS.Core.Models;

namespace DMS.Workflows.EmployeeWorkflows
{
    public class EmployeeUpdateStepHandler : BaseStepHandler
    {
        private readonly IEmployeeService _employeeService;
        private readonly DMS.Core.Models.Employee _employee;
        public EmployeeUpdateStepHandler(DMS.Core.Models.Employee employee)
        {
            _employee = employee;
            _employeeService = DependencyResolution.Resolve<IEmployeeService>();
        }
        public override bool Required => true;

        public override async Task<FunctionResult> ExecuteAsync(FunctionResult previousResults, CancellationToken cancellationToken)
        {
            var resultValid = previousResults;
            if (!resultValid.Succeed) return previousResults;

            var result = await _employeeService.UpdateEmployeeAsync(_employee, cancellationToken);
            return result;
        }
    }
}
